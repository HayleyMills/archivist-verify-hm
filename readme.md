## [Verify questionnaire metadata](https://wiki.ucl.ac.uk/display/CTTEAM/Metadata+Pipeline)


### input.txt
The archivist instance, for example: closer-archivist-alspac
The prefix, for example: alspac_04_tf1pi.  Note that this field can be blank, i.e. export PREFIX=


### output in csv format
|                                                  | Output Table         |
| :----------------------------------------------- | :------------------- |
| questionItem labels start with qi_               | qi\_label            |
| questionConstruct labels start with qc_          | qc\_label            |
| codeList labels start with cs_                   | cs\_label            |
| questionItems and questionConstruct labels match | qi\_qc\_label        |
| questions have a response                        | qi\_response         |
| question has a literal                           | qi\_literal          |
| Agency matches the CV                            | agency               |
| Study matches the agency                         | study\_agency        |
